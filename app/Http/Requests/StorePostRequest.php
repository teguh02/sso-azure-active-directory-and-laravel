<?php

namespace App\Http\Requests;

use App\Http\Requests\Abstracts\AbstractRequest;
use Illuminate\Foundation\Http\FormRequest;

class StorePostRequest extends AbstractRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true; # for example i set this manualy to true
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'name' => 'required|string',
            'phone' => 'required|string',
        ];
    }
}
