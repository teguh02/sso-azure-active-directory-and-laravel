<?php

namespace App\Http\Requests\Abstracts;

use Illuminate\Foundation\Http\FormRequest;

abstract class AbstractRequest extends FormRequest {
    public function attributes()
    {
        return [
            'name' => "User's Name",
            'phone' => "User's Phone Number",
        ];
    }
}